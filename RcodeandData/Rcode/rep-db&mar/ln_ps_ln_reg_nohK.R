common_path <- "D:/Projects/NonignorableMissingOutcome/nonignorablemissing/"
setwd(common_path)
source("Rcode/RepresenterFinal/main_functions_nloptr_nohK.R")
source("Rcode/RepresenterFinal/Hn_4cov_add.R")




data_generate <- function(n){
  X <- matrix(runif(n * 4), ncol = 4)
  # Y <- 20 + 10 * X - 12 * X^2 + rnorm(n)
  Y <- 1 + 2 * X[, 1] + 4 * X[, 2] + X[, 3] + 3 * X[, 4] + rnorm(n)
  
  eta_xy <- 3 + 2 * X[, 1] + X[, 2] + X[, 3] - 0.5 * X[, 4] - 0.8 * Y
  pr_obs <- exp(eta_xy) / (1 + exp(eta_xy))
  R <- rbinom(n, 1, prob = pr_obs)
  Y_obs <- Y
  Y_obs[R == 0] <- -9999
  Z <- 3 - 2 * X[, 1] + X[, 1]^2 + 4 * X[, 2] + 
    X[, 3] - 2 * X[, 4] + 3 * Y + rnorm(n)
  return(list(Y_true = Y, R = R, Y = Y_obs, Z = Z, X = X))
}





Times <- 10^5



n <- 500
set.seed(12345)
Data <- data_generate(10^7)
mu <- mean(Data$Y_true)
mu



K.set <- c(10^5, 10^3)
# K2.set <- c(10^3, 10^5)
gamma.set <- c(1, 1.2, 1.5, 5, 10)
# times <-  2
nfolds <- 5


trials <- 1000
hat_mu <- c()
hat_rn <- c()
hat_sigma <- c()
hat_cp <- c()
ci_level <- 0.95
hat_mu_corr <- c()
hat_mu_reg <- c()
filepath <- paste0(common_path, "/Results220906/sim_qdrupM/n=", n, "/")
makefolder(filepath)
filename <- paste0(filepath, "4cov_ln_ps_ln_reg_cp.txt")
whether_normalize = T


if(whether_normalize){
  Hn_mat <- Hn(0, 1, times = Times)
}else{
  set.seed(1)
  data <- data_generate(n)
  Hn_mat <- Hn(min(data$Z), max(data$Z), times = Times)
}



for(trial in 1:trials){
  try({
    set.seed(trial)
    cat("Trial = ", trial, " Starts!", "\n")
    start <- Sys.time()
    data <- data_generate(n)
    foldid = sample(rep(seq(nfolds), length = n))
    cvtuning <- cv.tuning(data, Hn_mat, K.set, gamma.set, polynomial_basis, 
                          whether_normalize = whether_normalize, foldid)
    K <- cvtuning$cvK
    gamma <- cvtuning$cvgamma
    mu_res <- mu_est(data, Hn_mat, K, gamma, polynomial_basis,
                     whether_normalize = whether_normalize)
    hat_mu[trial] <- mu_res$Hat_mu
    hat_beta0 <- mu_res$Hat_beta0
    hat_rn_res <- remainder_est(data, hat_mu[trial], Hn_mat, K, polynomial_basis, hat_beta0,
                                whether_normalize = whether_normalize)
    hat_rn[trial] <- hat_rn_res$Hat_rn
    hat_sigma_square <- hat_rn_res$Hat_sigma_square
    hat_sigma[trial] <- sqrt(hat_sigma_square / n)
    hat_mu_corr[trial] <- hat_mu[trial] + hat_rn[trial]
    hat_cp[trial] <- whether_coverage(hat_mu_corr[trial], mu, ci_level, hat_sigma_square, n)
    
    hat_mu_reg[trial] <- reg_est(data)
    
    
    end <- Sys.time()
    cost <- end - start
    cat("Trial = ", trial, " Ends! Hat_mu = ", hat_mu[trial], 
        ", Hat_mu_corr = ", hat_mu_corr[trial], ", Hat_rn = ",
        hat_rn[trial], ", Hat_cp = ", hat_cp[trial], ", Hat_sigma = ",
        hat_sigma[trial], ", Hat_mu_reg = ", hat_mu_reg[trial], 
        ", time costs = ", cost, "\n")
    write.table(cbind(trial, hat_mu[trial], hat_mu_corr[trial], 
                      hat_cp[trial], hat_sigma[trial],
                      hat_mu_reg[trial]), 
                file = filename, append = T,
                row.names = F, col.names = F)
  })
}

mean_hat_mu <- mean(hat_mu, na.rm = T) 
mean_hat_mu - mu

sd_hat_mu <- sd(hat_mu, na.rm = T)
sd_hat_mu

mean_hat_mu_corr <- mean(hat_mu_corr, na.rm = T)
mean_hat_mu_corr - mu

sd_hat_mu_corr <- sd(hat_mu_corr, na.rm = T)
sd_hat_mu_corr

mean_hat_cp <- mean(hat_cp, na.rm = T)
mean_hat_cp

mean_hat_sigma <- mean(hat_sigma, na.rm = T)
mean_hat_sigma

mean_hat_mu_reg <- mean(hat_mu_reg, na.rm = T)
mean_hat_mu_reg - mu

write.table(cbind(trials+1, mean_hat_mu, mean_hat_mu_corr, 
                  mean_hat_cp, mean_hat_sigma,
                  mean_hat_mu_reg), 
            file = filename, append = T,
            row.names = F, col.names = F)
