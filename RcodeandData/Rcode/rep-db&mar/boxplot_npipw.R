
trials <- 1000
boxplot_fun = function(savepath, results, truevalue, xlabels, xticks, 
                       ylim_low, ylim_up, yticks, colors){
  pdf(savepath, height=3.5, width=5)
  par(mfrow=c(1, 1), mar=c(2.3, 2.7, 1, 1), mgp = c(0.5, 0.7, 0))
  boxplot(results, outline = F,xaxt='n',boxwex=0.6, cex.main=2.5, 
          lwd = 1, yaxt = 'n',
          ylim=c(ylim_low, ylim_up),
          col = colors)
  # legend("bottomleft", legend = c("n = 500","n = 1000") , 
  #        col = c("white", "grey") , bty = "n", pch=20 , 
  #        pt.cex = 3, cex = 1, horiz = FALSE, inset = c(0.03, 0.1))
  abline(h = truevalue, col = "red")
  axis(1, at = xticks, tck=-0.03, labels = xlabels, cex.axis = 0.6, line = 0) 
  axis(2, at = yticks, labels = yticks, las = 1) 
  dev.off()
}

# ln_ps_ln_reg and nln_ps_ln_reg

common_path <- "D:/Projects/NonignorableMissingOutcome/nonignorablemissing/Results220906/"
results <- read.table(paste0(common_path, "sim_nohK/n=1000/4cov_nln_ps_ln_reg_cp.txt"))[1:trials, -1]

results_smallsize <- read.table(paste0(common_path, "sim_nohK/n=500/4cov_nln_ps_ln_reg_cp.txt"))[1:trials, -1]

qM_results <- read.table(paste0(common_path, "sim_qdrupM/n=1000/4cov_nln_ps_ln_reg_cp.txt"))[1:trials, -1]
qM_results_smallsize <- read.table(paste0(common_path, "sim_qdrupM/n=500/4cov_nln_ps_ln_reg_cp.txt"))[1:trials, -1]

lnipw_results <- read.table(paste0(common_path, "ipwln/n=1000/ipw_4cov_nln_ps_ln_reg.txt"))[1:trials, -1]
lnipw_results_smallsize <- read.table(paste0(common_path, "ipwln/n=500/ipw_4cov_nln_ps_ln_reg.txt"))[1:trials, -1]

npipw_results <- read.table(paste0(common_path, "ipwnp/n=1000/ipw_4cov_nln_ps_ln_reg.txt"))[1:trials, -1]
npipw_results_smallsize <- read.table(paste0(common_path, "ipwnp/n=500/ipw_4cov_nln_ps_ln_reg.txt"))[1:trials, -1]

ylim_low <- 5.1
ylim_up <- 6.4
truevalue <- 6
yticks <- seq(5.2, ylim_up, 0.4)

# expression(hat(mu)[adj])
xlabels <- c("qd-REP-DB", "qr-REP-DB", "ln-mnarIPW", "py-mnarIPW", "marREG")
xticks <- c(1.5, 3.5, 5.5, 7.5, 9.5)
colors <- rep(c("white", "grey"), 5)

results_all <- cbind(results_smallsize[, 2], results[, 2],
                     qM_results_smallsize[, 2], qM_results[, 2],
                     lnipw_results_smallsize[, 1], lnipw_results[, 1],
                     npipw_results_smallsize[, 1], npipw_results[, 1],
                     results_smallsize[, 5], results[, 5])

savepath = paste0(common_path, "all_nln_ps_ln_reg_220916.pdf")

boxplot_fun(savepath, results_all, truevalue, xlabels,
            xticks, ylim_low, ylim_up, yticks, colors)




# ln_ps_nln_reg and nln_ps_nln_reg


results1 <- read.table(paste0(common_path, "sim_nohK/n=1000/4cov_nln_ps_nln_reg_cp.txt"))[1:trials, -1]

results1_smallsize <- read.table(paste0(common_path, "sim_nohK/n=500/4cov_nln_ps_nln_reg_cp.txt"))[1:trials, -1]

qM_results1 <- read.table(paste0(common_path, "sim_qdrupM/n=1000/4cov_nln_ps_nln_reg_cp.txt"))[1:trials, -1]

qM_results1_smallsize <- read.table(paste0(common_path, "sim_qdrupM/n=500/4cov_nln_ps_nln_reg_cp.txt"))[1:trials, -1]

lnipw_results1 <- read.table(paste0(common_path, "ipwln/n=1000/ipw_4cov_nln_ps_nln_reg.txt"))[1:trials, -1]
lnipw_results1_smallsize <- read.table(paste0(common_path, "ipwln/n=500/ipw_4cov_nln_ps_nln_reg.txt"))[1:trials, -1]

npipw_results1 <- read.table(paste0(common_path, "ipwnp/n=1000/ipw_4cov_nln_ps_nln_reg.txt"))[1:trials, -1]
npipw_results1_smallsize <- read.table(paste0(common_path, "ipwnp/n=500/ipw_4cov_nln_ps_nln_reg.txt"))[1:trials, -1]


ylim_low1 <- 5.2
ylim_up1 <- 6.4
truevalue <- 6.063358
yticks1 <- seq(5.2, ylim_up1, 0.4)

# expression(hat(mu)[adj])

xlabels <- c("qd-REP-DB", "qr-REP-DB", "ln-mnarIPW", "py-mnarIPW", "marREG")
xticks <- c(1.5, 3.5, 5.5, 7.5, 9.5)
colors <- rep(c("white", "grey"), 5)

results1_all <- cbind(results1_smallsize[, 2], results1[, 2],
                     qM_results1_smallsize[, 2], qM_results1[, 2],
                     lnipw_results1_smallsize[, 1], lnipw_results1[, 1],
                     npipw_results1_smallsize[, 1], npipw_results1[, 1],
                     results1_smallsize[, 5], results1[, 5])

savepath1 = paste0(common_path, "all_nln_ps_nln_reg_220916.pdf")

boxplot_fun(savepath1, results1_all, truevalue, xlabels,
            xticks, ylim_low1, ylim_up1, yticks1, colors)


