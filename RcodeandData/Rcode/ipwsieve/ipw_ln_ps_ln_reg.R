common_path <- "D:/Projects/NonignorableMissingOutcome/nonignorablemissing/"
setwd(common_path)
source("Rcode/IPWnonpara/ipwnp_bbsolve_main_functions.R")




data_generate <- function(n){
  X <- matrix(runif(n * 4), ncol = 4)
  # Y <- 20 + 10 * X - 12 * X^2 + rnorm(n)
  Y <- 1 + 2 * X[, 1] + 4 * X[, 2] + X[, 3] + 3 * X[, 4] + rnorm(n)
  
  eta_xy <- 3 + 2 * X[, 1] + X[, 2] + X[, 3] - 0.5 * X[, 4] - 0.8 * Y
  pr_obs <- exp(eta_xy) / (1 + exp(eta_xy))
  R <- rbinom(n, 1, prob = pr_obs)
  Y_obs <- Y
  Y_obs[R == 0] <- -9999
  Z <- 3 - 2 * X[, 1] + X[, 1]^2 + 4 * X[, 2] + 
    X[, 3] - 2 * X[, 4] + 3 * Y + rnorm(n)
  return(list(Y_true = Y, R = R, Y = Y_obs, Z = Z, X = X))
}








n <- 500
set.seed(12345)
Data <- data_generate(10^7)
mu <- mean(Data$Y_true)
mu



trials <- 1000
hat_mu <- c()
hat_cp <- c()
filepath <- paste0(common_path, "/Results/ipwnp/n=", n, "/")
makefolder(filepath)
filename <- paste0(filepath, "ipw_4cov_ln_ps_ln_reg.txt")
whether_normalize = T





for(trial in 1:trials){
  try({
    set.seed(trial)
    cat("Trial = ", trial, " Starts!", "\n")
    start <- Sys.time()
    data <- data_generate(n)
    # initial <- c(mu, -0.8, 3, 2, 1, 1, -0.5)
    initial <- c(mu, -0.8, 0, 0, 3, 2, 1, 1, -0.5, rep(0, 8))
    # initial <- Initial_est(data)
    ipw_res <- ipw_est(data, GMMF, IPWmrf, initial, mu)
    hat_mu[trial] <- ipw_res$IPWmu
    hat_cp[trial] <- ipw_res$IPWcvr
    
    
    end <- Sys.time()
    cost <- end - start
    cat("Trial = ", trial, " Ends! Hat_mu = ", hat_mu[trial], 
        ", Hat_cp = ", hat_cp[trial],  
        ", time costs = ", cost, "\n")
    write.table(cbind(trial, hat_mu[trial], hat_cp[trial]), 
                file = filename, append = T,
                row.names = F, col.names = F)
  })
}

mean_mu <- mean(hat_mu, na.rm = T) - mu 
mean_mu
sd(hat_mu, na.rm = T)


mean_cp <- mean(hat_cp, na.rm = T)
mean_cp

write.table(cbind(trials + 1, mean_mu, mean_cp), 
            file = filename, append = T,
            row.names = F, col.names = F)
